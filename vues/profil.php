<?php
    include('modules/partie1.php');
?>
<?php
// Import et instanciation de la calsse Database
require_once(__DIR__."/../models/database.php");
$database = new Database();

// Recherche du user dans la session et désérialisation
$user = unserialize($_SESSION["user"]);
?>

<div class="container card text-center mt-4">
    <h1 class="card-header">Bienvenue <?php echo $user->getNom(); ?></h1>
    <div class="card-body text-left">
        <div class="card-title">
            Vous êtes inscrit aux cours suivants :
        </div>
        <div class="card-text">
            <ul class="inscrit text-dark text-left">
                <?php foreach($seance as $seance){ ?>
                <a href="/vues/cours.php?id=<?php echo $seance->getId(); ?>">
                <li><i class= "fa fa-bookmark"></i>
                <?php echo $seance->getTitre().", le " .date("d/m/Y", strtotime($seance->getDate())).
                " à " .date("G\hi", strtotime($seance->HeureDebut())) ?></li></a>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>

<?php

    include('modules/partie3.php');

// ON redirige l'utilisateur vers la page login s'il n'est pas connecté
if(!isset($_SESSION["user"])){
    // Comme notre page est déjà partiellement construite on ne peut pas utiliser header()
    // On va donc utiliser un petit script javascript pour se rediriger
    echo '<script type="text/javascript">';
    echo 'window.location.href="login.php";';
    echo '</script>';
}

// Recherche du user dans la session et désérialisation
$user = unserialize($_SESSION["user"]);
?>


                