<?php
    include('modules/partie1.php');
    ?>

<div class="container card text-center mt-4">
    <h1 class="card-header">Bienvenue chez le Club Lambda</h1>
    <div class="card-body">
        <img class="mainImage" src="/vues/assets/images/about-fitness-img.png">
        <p class="p-4">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
        Nulla sodales pellentesque massa. Ut porta ac augue ultrices viverra. 
        Nam malesuada vestibulum nunc nec sollicitudin. Curabitur ut nunc laoreet, molestie libero eu, sagittis ante. 
        Ut lobortis maximus risus, non finibus neque scelerisque ut. Morbi vel lobortis dolor. 
        Aliquam erat volutpat. Donec blandit, sem et dictum pulvinar, odio felis feugiat nisl, 
        ut vulputate lectus odio sit amet nisi. Sed ex dolor, pretium at risus sed, 
        interdum mattis neque. Proin sed nisl nec est dictum ultrices. 
        Praesent laoreet magna eget augue mollis, nec consectetur ex blandit. 
        Mauris risus augue, consectetur in ex rutrum, pellentesque rutrum mi.
        </p>
        <a class="btn btn-dark" href="#">Consulter le planning</a>
    </div>
</div>

<?php
    include('modules/partie3.php');
?>
