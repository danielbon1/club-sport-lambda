<?php
    include('modules/partie1.php');
?>

<div class="container card text-center mt-4">
    <h1 class="card-header">Inscription</h1>
    <div class="card-body">
        <form class="text-left md-right" action="process/inscription.php" method="POST">
            <div class="form-group row">
                <label for="name" class="col-sm-12 col-md-4 col-form-label">Nom</label>
                <div class="col-sm-12 col-md-8">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Nom et prénom" required>
                </div>
            </div>
            <div class="form-group row">
            <label for="email" class="col-sm-12 col-md-4 col-form-label">Email</label>
                <div class="col-sm-12 col-md-8">
                    <input type="email" class="form-control" id="email" name="email" 
                    placeholder="Email" required>
                </div>
            </div>
            <div class="form-group row">
            <label for="password" class="col-sm-12 col-md-4 col-form-label">Mot de passe</label>
                <div class="col-sm-12 col-md-8">
                    <input type="password" class="form-control" id="password" name="password" 
                    placeholder="Mot de passe" required>
                </div>
            </div>
            <div class="form-group row">
            <label for="password-repeat" class="col-sm-12 col-md-4 col-form-label">Retapez le
            mot de passe</label>
                <div class="col-sm-12 col-md-8">
                    <input type="password" class="form-control" id="password-repeat" name="password-repeat" 
                    placeholder="Retapez le mot de passe" required>
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-dark" type="submit">S'inscrire</button>
            </div>
        </form>
    </div>
</div>

<?php
    include('modules/partie3.php');
?>



