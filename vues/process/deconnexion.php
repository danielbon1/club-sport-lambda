<?php
// Ce fichier sert à déconnecter

// On va utiliser la session pour passer des messages d'une page à l'autre
// POur cela il faut démarrer la session au début des pages concernées
session_start();

// On vide la session
$_SESSION = [];

// On redirige vers la page d'acceuil
header("location: ../index.php");
exit();
