<?php
// Ce fichier sert à désinscrire un user à une séance

// On va utiliser la session pour passer ds messages d'une page à l'autre
//Pour cela il faut démarrer la session au début des pages concernées
session_start();

require_once(__DIR__ ."//../models/Database.php");
$database = new Database();

// Récupérer l'id du user de la séance dans l'url
$idSeance = $_GET["id"];

// Récupérer l'id du usd dans la session
$idUser = $_SESSION["id"];

// Effectuer l'inscription en base de données
if($database->deleteParticipant($idSeance, $idUser)){
    // Si ça c'est bien passé
    $_SESSION["info"] = "Vous aves bien été désinscrit de cette séance";
}else{
    // SI ça c'est mal passé
    $_SESSION["error"] = "Nous n'avons pas réussi à vous désinscrire de cette séance";
}
header("location: ../vues/cours.php?id=".$idSeance);
exit();
