<?php
// Ce fichier seet à processer les données du formulaire

// On va utiliser la session pour passer des messages d'une page à l'autre
// Pour cela il faut démarrer la session au début des pages concernées
session_start();

require_once(__DIR__."/../../models/Database.php");
$database = new Database();

// Récupération des données du formualaire d'inscription
$nom = isset($_POST["name"]) ? $_POST["name"] : null;
$email = isset($_POST["email"]) ? $_POST["email"] : null;
$password = isset($_POST["password"]) ? $_POST["password"] : null;
$passwordRepeat = isset($_POST["password-repeat"]) ? $_POST["password-repeat"] : null;

// Validation des données reçues
$errors = "";
if($nom == null){
    $errors .= "Le nom d'utilisateur doit être rempli";
}
if($email == null){
    $errors .= "L'email doit être renseigné";
}
if ($database->isEmailExists($email)){
    $errors .= "Cet email existe déjà";
}
if($password == null){
    $errors .= "Le mot de passe est obligatoire";
}
if($passwordRepeat == null || $passwordRepeat != $password){
    $errors .= "Vous devez répéter le mot de passe";
}

// En cas d'erreurs rediriger vers le formulaire
if(!empty($errors)){
    $_SESSION["error"] = $errors;
    header("location: ../inscription.php");
    exit();
}

// S'i n'y a pas d'erreurs on crée l'utilisateur en mode inactif
// On sauvegarde le token dans una variable pour le réutiliser
$token = bin2hex(random_bytes(20));
$user = User::createUser($nom, $email, password_hash($password, PASSWORD_DEFAULT), 0, 0, $token);

// On sauvegarde l'utilisateur et on récupère on id
$idUser = $database->createUser($user);

// Préparation pour la 2ème partie
// On doit envoyer un email

if($idUser){
    // Créons le message et le sujet de l'email
    // On ouvre un buffer, on inclut le message puis on vide le buffer dans notre variable message
    ob_start();
    include("../emails/activation.php");
    $message = ob_get_clean();
    $sujet ="Activer votre compte Lambda";

    // Pour que le message s'affiche en HTML il faut le spécifier dans le header de l'email
    // On peut aussi spécifier la personne qui envoie l'email
    $headers = 'Content-Type: text/html; charset=utf-8' . "r\n";
    $headers .= 'From: noreply@club-lambda.ch';

    // Et on envoie l'email
    mail($email, $sujet, $message, $headers);

    // On redirige vers l'écran de login avec un message
    $_SESSION["info"] = "Votre inscription a bien été prise en compte,
                            vous avez reçu un email pour activer votre compte";
    header("location: ../login.php");
}else{
    // Une erreur s'est produite pendant la création en base de données
    $_SESSION["error"] = "Une erreur s'est produite lors de votre inscription,
                            veuillez recommencer le processus";
    header("location: ../inscription.php");
}


