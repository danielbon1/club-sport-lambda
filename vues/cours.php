<?php
include('modules/partie1.php');
?>

<?php
// J'instencie la nouvelle Database
require_once(__DIR__ . "/../models/Database.php");
$database = new Database();

// Je récupère l'id dans l'url
// (Si pas d'id dans l'url on prendra 1 par défaut)
$idSeance = isset($_GET["id"]) ? $_GET["id"] : 1;

// Je vais chercher la séance dans la base de données
$seance = $database->getSeanceById($idSeance);
?>

<div class="container card text-center mt-4">
    <h1 class="card-header"><?php echo $seance->getTitre(); ?></h1>
    <div class="card-body text-left" style="background: <?php echo $seance->getCouleur() ?>;">
        <div class="row">
            <div class="offset-3 col-9">
                <p>Date : <?php echo $seance->getDate(); ?></p>
            </div>
            <div class="offset-3 col-9">
                <p>Heure de début : <?php echo $seance->getheureDebut(); ?></p>
            </div>
            <div class="offset-3 col-9">
                <p>Durée : <?php echo $seance->getDuree(); ?></p>
            </div>
            <div class="offset-3 col-9">
                <p>Description :</p>
            </div>
            <div class="offset-3 col-9">
                <p><?php echo $seance->getDescription(); ?></p>
            </div>
            <div class="offset-3 col-9">
                <p>Nombre de participants max : <?php echo $seance->getNbParticipantsMax(); ?></p>
            </div>
    <?php
    // Déserialisation du user pour savoir s'il est admin ou pas
    $user = unserialize($_SESSION["user"]);

    // Récuperation du nombre d'inscrits
    $nbInscrits = $database->nombreInscrits($idSeance);

    // Est ce que le user est déjà inscrit ou pas
    $isInscrit = $database->isInscrit($user->getId(), $idSeance);
    ?>
            <div class="col-12">
                <div class="d-flex justify-content-around m-2">
                <?php if($isInscrit){ ?>
                    <a class="btn btn-danger" href="../process/desinscription-seance.php?id=
                <?php echo $seance->getId(); ?>">Se désinscrire</a>
                <?php }else if($nbInscrit < $seance->getNbParticipantsMax()){ ?>
                    <a class="btn btn-primary" href="../process/inscription-seance.php?id=
                    <?php echo $seance-> getId(); ?>">S'inscrire</a>
                <?php }else{ ?>
                    <a class="btn btn-danger" href="#">Complet</a>
                <?php } //endif ?>
                </div>
            </div>
            <?php if($user->isAdmin() == 1) { ?>
            <div class="col-12">
                <div class="d-flex justify-content-around m-2">
                    <a class="btn btn-warning" href="/vues/formulaire.php?id=<?php echo $seance->getId(); ?>&type=3">Dupliquer</a>
                    <a class="btn btn-primary" href=" ../vues/formulaire.php?id=<?php echo $seance->getId(); ?>&type=2">Modifier</a>
                    <a class="btn btn-danger" href=" ../process/delete-seance.php?id=<?php echo $seance->getId(); ?>">Supprimer</a>
                </div>
            </div>
            <?php } //endif ?>
        </div>
    </div>
</div>

<?php
include('modules/partie3.php');
?>