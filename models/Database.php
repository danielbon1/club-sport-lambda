<?php
/**
 * Classe de connexion à la base de donnée
 */
include_once('Seance.php');
include_once('User.php');

class Database
{

    //Constante de connexion
    const DB_HOST = "mariadb";
    const DB_PORT = "3306";
    const DB_NAME = "clublambda";
    const DB_USER = "adminClub";
    const DB_PASSWORD = "Debora01";

    // Attribut de la classe
    private $connexion;

    // Constructeur pour initier la connexion
    public function __construct()
    {
        try {
            $this->connexion = new
                PDO(
                    "mysql:host=" . self::DB_HOST . ";port=" . self::DB_PORT . ";dbname=" . self::DB_NAME . ";charset=UTF8",
                    self::DB_USER,
                    self::DB_PASSWORD
                );
        } catch (PDOException $e) {
            echo 'Connexcion échouée : ' . $e->getMessage();
        }
    }
    /**
     * Fonction pour créer une nouvelle séance en base de données
     * 
     * @param{Seance} seance : la séance à sauvegarder
     * 
     * @return{integer, boolean} l'id si la séance a été crée ou false sinon
     */
    // Je prepare ma requête SQL
    public function createSeance(Seance $seance)
    {

        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO seances(titre, description, heureDebut, date, duree, nbParticipantsMax, couleur)
            VALUES (:titre, :description, :heureDebut, :date, :duree, :nbParticipantsMax, :couleur)"
        );
        // J'exécute ma requête en passant les valeurs de l'objet Seance en valeur
        $pdoStatement->execute([
            "titre"                 => $seance->getTitre(),
            "description"           => $seance->getDescription(),
            "heureDebut"            => $seance->getHeureDebut(),
            "date"                  => $seance->getDate(),
            "duree"                 => $seance->getDuree(),
            "nbParticipantsMax"     => $seance->getNbParticipantsMax(),
            "couleur"               => $seance->getCouleur()

        ]);

        // Je récupère l'id créé si l'exécution s'est bien passée (code 0000 de MySQL)
        if ($pdoStatement->errorCode() == 0) {
            $id = $this->connexion->lastInsertId();
            return $id;
        } else {

            return false;
        }
    }
    /**
     * Cette fonction cherche la seance dont l'id est passé en paramètre et la retourne
     * 
     * @param{integer} id : l'id de la séance recherchée
     * 
     * @return{Seance|boolean} : un objet Seance si la séance a été trouvée, false sinon
     */
    // Je prépare ma requête SQL
    public function getSeanceById($id)
    {
    
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM seances WHERE id = :id"
        );
        // J'exécute la requête en lui passant l'id
        $pdoStatement->execute(
            ["id" => $id]
        );
        //Je récupère le résultat
        $seance = $pdoStatement->fetchObject("Seance");
        return $seance;
    }
    /**
     * Fonction retourne toutes les séances de la semaine
     * 
     * @param{integer} week : le numéro de la semaine recherchée
     * 
     * @return{array} : un tableau de Seance s'il y a des séances programmées pour cette semaine
     */
    // Je préprare ma requête SQL
    public function getSeanceByWeek($week)
    {
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM seances WHERE WEEKOFYEAR(date) = :week
            ORDER By date, heureDebut"

        );
        // J'exécute la requête en lui passant le numéro de la semaine
        $pdoStatement->execute(
            ["week" => $week]
        );
        // Je récupère les resultats
        $seances = $pdoStatement->fetchAll(PDO::FETCH_CLASS, "Seance");
        return $seances;
    }
    // Je prépare ma requête SQL
    public function deleteAllSeance()
    {
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM seances;"
        );
        $pdoStatement->execute();
    }
    /**
     * Fonction pour mettre à jour une séance en base de données
     * 
     * @param{Seance} seance : la séance à mettre à jour
     * 
     * @return{boolean} true si la séance est mise à jour ou false sinon
     */
    // Je prépare ma requête 
    public function updateSeance(Seance $seance)
    {
        $pdoStatement = $this->connexion->prepare(
            "UPDATE seances
            SET titre = :titre, description = :description, heureDebut = :heureDebut,
            date = :date, duree = :duree, nbParticipantsMax = :nbParticipantsMax, couleur = :couleur
        WHERE id = :id"
        );
        // J'exécute ma requête en passant les valeurs de l'objet Seance en valeur
        $pdoStatement->execute([
            "titre"                 => $seance->getTitre(),
            "description"           => $seance->getDescription(),
            "heureDebut"            => $seance->getHeureDebut(),
            "date"                  => $seance->getDate(),
            "duree"                 => $seance->getDuree(),
            "nbParticipantsMax"     => $seance->getNbParticipantsMax(),
            "couleur"               => $seance->getCouleur(),
            "id"                    => $seance->getId()

        ]);
        // Retourne true créé si l'exécution s'est bien passée (code 0000 de MySQL)
        if ($pdoStatement->errorCode() == 0) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * Fonction pour supprimer une séance en base de données
     * 
     * @param{integer} id : l'id de la séance à supprimer
     * 
     * @return{boolean} true si la séance est supprimée ou false sinon
     */
    // Je prépare la requête pour supprimer tou les inscrits à la séance
    public function deleteSeance($id)
    {
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM inscrits WHERE id_seance = :seance"
        );
        // J'exécute ma requête
        $pdoStatement->execute(
            ["seance" => $id]
        );
        
        // Si ça ne s'est pas bien passé ce n'est pas la peine de continuer
        if($pdoStatement->errorCode() != 0) {
            return false;
        }
        
        // Si les inscrits sont supprimés, je prépare la requête pour supprimer la séance
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM seances WHERE id = :seance"
        );
        
        // J'exécute ma requête
        $pdoStatement->execute(
            ["seance" => $id]
        );
        // Retourne true créé si l'exécution s'est bien passée (code 0000 de MySQL)
        if ($pdoStatement->errorCode() == 0){
            return true;
        } else {
            return false;
        }
    }
    // Je prépare la requêete d'insertion
    public function insertParticipant($idSeance, $idUser){
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO inscrits (id_user, id_seance)
            VALUES (:id_user, :id_seance)"
        );
        // J'exécute ma requête
        $pdoStatement->execute(
            ["id_user" => $idUser,
            "id_seance" => $idSeance]
        );
        // retourne true créé si l'exécution s'est bien passée (code 0000 de MySQL)
        if($pdoStatement->errorCode() == 0){
            return true;
        }else{
            return false;
        }
    }
    // Je prépare la requêete d'insertion
    public function deleteParticipant($idSeance, $idUser){
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM inscrits WHERE id_user = :id_user AND id_seance = :id_seance"
        );
        // J'exécute ma requête
        $pdoStatement->execute(
            ["id_user" => $idUser,
            "id_seance" => $idSeance]
        );
        // retourne true créé si l'exécution s'est bien passée (code 0000 de MySQL)
        if($pdoStatement->errorCode() == 0){
            return true;
        }else{
            return false;
        }
    }
    /**
     * 
     * @param{User} user : le user à sauvegarder
     * 
     * @return{integer, boolean} l'id si le user a été créé ou false sinon
     */
    // Je prépare ma requête SQL
    public function createUser(User $user){
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO users(nom, email, password, isAdmin, isActif, token)
            VALUES (:nom, :email, :password, :isAdmin, :isActif, :token)"
        );
        // J'exécute ma requête en passant les valeurs de l'objet User en valeur
        $pdoStatement->execute([
            "nom"           => $user->getNom(),
            "email"         => $user->getEmail(),
            "password"      => $user->getPassword(),
            "isAdmin"       => $user->isAdmin(),
            "isActif"       => $user->isActif(),
            "token"         => $user->getToken()
        ]);

        // Je récupère l'id créé si l'exécution s'est bien passée (code 0000 MySQL)
        if($pdoStatement->errorCode() == 0){
            $id = $this->connexion->lastInsertId();
            return $id;
        }else{
            return false;
        }

    }
    // Je prépare ma requête SQL
    public function deleteAllUser(){
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM users;"
        );
        $pdoStatement->execute();

    }
    // Je prépare ma requête SQL
    public function deleteAllInscrit(){
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM inscrits;"
        );
        $pdoStatement->execute();

    }
    /**
     * Cette fonction cherche le user dont l'id est passé en paramètre et le retourne
     * 
     * @param{integer} id : l'id du user recherché
     * 
     * @return{User|boolean} : un objet User si le user a été trouvé, false sinon
     */
    // Je prépare ma requête SQL
    public function getuserById($id){
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM users WHERE id = :id"
        );
        // J'exécute la requête en lui passant l'id
        $pdoStatement->execute(
            ["id" => $id]
        );
        // Je récpupere le résultat
        $user = $pdoStatement->fetchObject("User");
        return $user;
    }
    /**
     * Active le user dont l'id est pass en paramètre
     * 
     * @param{integer} id : l'id du user à activer
     * 
     * @return{boolean} true si l'activation s'est bien passée, false sinon
     */
    // Je prepare ma requête 
    public function activateUser($id){
        $pdoStatement = $this->connexion->prepare(
            "UPDATE users
            SET isActif = 1
            WHERE id = :id"
        );
    // J'exécute ma requête en passant l'id en valeur
    $pdoStatement->execute([
        "id" => $id
    ]);
    // Retourne true créé si l'exécution s'est bien passée (code 0000 MySQL)
    if($pdoStatement->errorCode() == 0){
        return true;
    }else{
        return false;
    }
    }
    /**
     * Vérifie si un email existe déjà dans un tableau users
     * 
     * @param{string} email : un email utlisé pour s'inscrire
     * 
     * @return{boolean} : true si l'email existe déjà, false sinon
     */
    // Je prépare ma requête SQL
    public function isEmailExists($email){
        $pdoStatement = $this->connexion->prepare(
            "SELECT COUNT(*) FROM users WHERE email = :email"
        );
        // J'exécute la requ'ete en lui passant l'email en valeur
        $pdoStatement->execute(
            ["email" => $email]
        );
        // Je récupère le résultat
        $nbUser = $pdoStatement->fetchColumn();
        // Si l'email n'a pas été trouvé retourne false
        if($nbUser == 0){
            return false;
        }else{
            // L'email a été trouvé
            return true;
        }
    }
    /**
     * Cette fonction cherche le user dont l'email et passé en paramètre et le retourne
     * 
     * @param{string} email : l'emai du user recherché
     * 
     * @return{User|boolean} : un objet User si le user a été trouvé, false sinon
     */
    // Je prépare ma requête SQL
    public function getUserByEmail($email){
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM users WHERE email = :email"
        );
        // J'exécute la requête en lui passant l'id
        $pdoStatement->execute(
            ["email" => $email]
        );
        // Je récupère le résultat
        $user = $pdoStatement->fetchObject("User");
            return $user;
        }
     /**
     * Fonction qui permet de retrouver toute les séances auxquelles est inscrit le user
     * 
     * @param{integer} id : l'id du user concerné
     * 
     * @return{array} : un tableau contenant toutes les séances
     */
    // Je prépare ma requête SQL
    public function getSeanceByUserId($idUser){
        $pdoStatement = $this->connexion->prepare(
            "SELECT s.* FROM seances s INNER JOIN inscrits i ON s.id = i.id_seance 
            WHERE i.id_user = :id_user"
        );
        // J'exécute la requête en lui passant l'id du user
        $pdoStatement->execute(
            ["id_user" => $idUser]
        );
        // Je récupère les résultats
        $seances = $pdoStatement->fetchAll(PDO::FETCH_CLASS,"Seance");
            return $seances;
        }
    /**
 * Fonctiom qui nous permet de savoir si un utilisateur est inscrit à une séance
 * 
 * @param{integer} idUser : l'id de l'utilisateur
 * @param{integer} isSeance : l'id de la séance
 * 
 * @return(boolean) true si l'utilisateur est inscrit, false sinon
 */
public function isInscrit($idUser, $idSeance){
    // Je prepare la requête d'insertion
    $pdoStatement = $this->connexion->prepare(
        "SELECT COUNT(*) FROM inscrits WHERE id_user = :id_user AND id_seance = :id_seance"
    );
    // J'exécute ma requête
    $pdoStatement->execute(
        ["id_user" => $idUser,
        "id_seance" => $idSeance]
    );
    // Je récupère le résultat
    $inscrit = $pdoStatement->fetchColumn();
    // Si aucune inscription n'a été trouvée retourne false
    if($inscrit == 0){
        return false;
    }else{
        // Une inscription a été trouvée
        return true;
    }     
}

/**
 * Fonction qui retrouve le nombre d'inscrit à une séance
 * 
 * @param{integer} idSeance : l'id de la séance
 * 
 * @return{integer} le nombre d'inscrits
 */
public function nombreInscrits($idSeance){
    // Je prépare la requête d'insertion
    $pdoStatement = $this->connexion->prepare(
        "SELECT COUNT(*) FROM inscrits WHERE id_seance = :id_seance"
    );
    // J'exécute ma requête
    $pdoStatement->execute(
        ["id_seance" => $idSeance]
    );
    // Je récupère le résultat
    $nbInscrits = $pdoStatement->fetchColumn();
    // Retourne le nombre d'inscrits à cette séance
    return $nbInscrits;
}

}