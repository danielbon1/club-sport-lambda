<?php

//Attribut de la classe
class User {
    private $id;
    private $nom;
    private $email;
    private $password;
    private $isAdmin;
    private $isActif;
    private $token;

// Constructeur par défaut
public function __construct(){}

// Constructeur acceptant toutes les valeurs de l'objet
public static function createUser($nom, $email, $passowrd, $isAdmin, $isActif, $token){
    $user = new self();
    $user->setNom($nom);
    $user->setEmail($email);
    $user->setPassword($passowrd);
    $user->setAdmin($isAdmin);
    $user->setActif($isActif);
    $user->setToken($token);
    return $user;
}

// Getters
public function getId(){ return $this->id; }
public function getNom(){ return $this->nom; }
public function getEmail(){ return $this->email; }
public function getPassword(){ return $this->password; }
public function isAdmin(){ return $this->isAdmin; }
public function isActif(){ return $this->isActif; }
public function getToken(){ return $this->token; }

// Setters
public function setId($id){ $this->id = $id; }
public function setNom($nom){ $this->nom = $nom; }
public function setEmail($email){ $this->email = $email; }
public function setPassword($password){ $this->password = $password; }
public function setAdmin($isAdmin){ $this->isAdmin = $isAdmin; }
public function setActif($isActif){ $this->isActif = $isActif; }
public function setToken($token){ $this->token = $token; }

}

?>